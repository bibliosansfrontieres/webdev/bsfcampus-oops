# BSFCampus Oops

Because the service is gone for now,
we need a landing page while we are rebuilding the platform.

## Deployment

`formation.bsfcampus.org` and `front.bsfcampus.org` are `CNAME`s pointing at `bubble`.


* Vhost: `/etc/nginx/sites-available/bsfcampus`
* DocumentRoot: `/var/www/formation.bsfcampus.org/`

